This is a collection of Batch scripts for enabling and disabling finger input on Windows 8. They actually modify the registry at HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Wisp\Touch

You need to restart the computer after running one of the scripts.

## References
* [http://www.robvanderwoude.com/regedit.php](http://www.robvanderwoude.com/regedit.php)
* [How do you disable finger input in Windows 8](http://answers.microsoft.com/en-us/windows/forum/windows_8-tms/how-do-you-disable-finger-input-in-windows-8/6c3bbe48-8703-4ea6-94dd-939c0418fcb1)